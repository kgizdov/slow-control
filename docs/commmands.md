# PDAQ Slow Control Commands

#### Status commands:

| Syntax         |       | Returns                                                                                                   |
| :------------- | :---: | :-------------------------------------------------------------------------------------------------------: |
| Status?        | ->    | `<hv on\/off\/interlock>`,`<hv>`V,`<hv current>`mA, `<led1 on/off>`,`<bias1>`V,`<led2 on/off>`,`<bias2>`V |
| Status:HV?     | ->    | `<on/off/interlock>`,`<hv>`V,`<hv current>`mA                                                             |
| Status:LED1?   | ->    | `<on/off>`,`<bias1>`V                                                                                     |
| Status:LED2?   | ->    | `<on/off>`,`<bias2>`V                                                                                     |
| Status:HvRamp? | ->    | `<ramp state>`,`<error>`,`<set point>` V,`<slew rate>` V/s                                                |
| Sensors?       | ->    | `<sensor1 ID>`,`<sensor2 ID>`,...........                                                                 |
| Temperature?   | ->    | `<sensor1 ID>`,`<temperature>`°C,`<sensor2 ID>`,...........                                               |
| Humidity?      | ->    | `<temperature>`°C,`<humidity>`%                                                                           |

#### To enable SET commands:

| Syntax                              |       | Returns    |
| :---------------------------------- | :---: | :--------: |
| Password:`<password>`               | ->    | OK         |

#### SET commands:

| Syntax                              |       | Returns    |
| :---------------------------------- | :---: | :--------: |
| Set:HV `<value>` `[,<slew rate>]`\* | ->    | OK         |
| Set:HVON `<0/1>`                    | ->    | OK         |
| Set:Led1 `<value 0.0-5.0>`          | ->    | OK         |
| Set:Led2 `<value 0.0-5.0>`          | ->    | OK         |
| Set:EnableLed1 `<0/1>`              | ->    | OK         |
| Set:EnableLed2 `<0/1>`              | ->    | OK         |


\* slew rate is an optional argument


#### Errors:

| Syntax                              |       | Returns                      |
| :---------------------------------- | :---: | :--------------------------: |
| Any command can return errors       | ->    | ERROR:`<number>`,`<message>` |
