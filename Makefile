CPP = g++
CPPFLAGS = -Wall -I.
LIBS = -lpthread

# OBJECTS =
# TARGET =

# $^ - objects
# $@ - target

all: dir_struct ./bin/slow_control message

./bin/slow_control: ./lib/source.o ./lib/easywsclient.o
	$(CPP) $(CPPFLAGS) $^ -o $@ $(LIBS)

./lib/%.o: ./src/%.cpp
	$(CPP) $(CPPFLAGS) -c $^ -o $@ $(LIBS)

run:
	@sudo ./bin/slow_control

dir_struct:
	@mkdir -p bin lib

message:
	@echo '#########################################'
	@echo '############### COMPLETED ###############'
	@echo '#########################################'

clean:
	@rm -f ./lib/* ./bin/*
