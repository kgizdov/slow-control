// Copyright Konstantin Gizdov 2016 Uni Edinburgh

#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>

#include <cstdlib>
#include <string>
#include <iostream>

#include "../include/easywsclient.hpp"

using easywsclient::WebSocket;
static WebSocket::pointer ws = NULL;

pthread_mutex_t wait_ = PTHREAD_MUTEX_INITIALIZER;

void handle_message(const std::string & message) {
    // pthread_mutex_lock(&wait_);
    std::cout << message.length() << std::endl;
    // while (message.length() <= 1) {
    //     std::cout << "CHECK" << std::endl;
    //     ws->poll();
    // }
    printf(">>> %s\n", message.c_str());
    // ws->poll();
    // sleep(1);
    // if (message == "world") { ws->close(); }
    // ws->close();
    // pthread_mutex_unlock(&wait_);
    return;
}

int main(int argc, char const *argv[]) {
    if (argc <= 2 || argc > 3) {
        std::cerr << "USAGE: slow_control <IP> <PORT>" << std::endl;
        exit(-1);
    }
    std::string first, ip, port;
    first = "ws://";
    ip = std::string(argv[1]);
    port = std::string(argv[2]);
    // ws = WebSocket::from_url("ws://192.168.0.10:4444/");
    ws = WebSocket::from_url(first + ip +":" + port);
    assert(ws);
    pthread_mutex_lock(&wait_);
    ws->send("Status?");
    ws->poll();
    pthread_mutex_unlock(&wait_);
    // ws->send("hello");
    // pthread_mutex_lock(&wait_);
    if (ws->getReadyState() != WebSocket::CLOSED) {
        std::cout << "CHECK" << std::endl;
        // sleep(1);
        // pthread_mutex_lock(&wait_);
        // ws->poll();
        // pthread_mutex_unlock(&wait_);
        pthread_mutex_lock(&wait_);
        ws->dispatch(handle_message);
        pthread_mutex_unlock(&wait_);
    }
    // pthread_mutex_unlock(&wait_);
    while (ws->getReadyState() != WebSocket::CLOSED && ws->getReadyState() != WebSocket::CLOSING) {
        int k;
        std::cout << "Enter command: " << std::endl;
        std::cout << "(1) Status, (2) Status HV, (3) Status LED1, (4) Quit: ";
        std::cin >> k;
        // pthread_mutex_lock(&wait_);
        // ws->poll();
        // pthread_mutex_unlock(&wait_);
        switch (k) {
            case 1: {
                pthread_mutex_lock(&wait_);
                ws->send("Status?");
                ws->poll();
                pthread_mutex_unlock(&wait_);
                // pthread_mutex_lock(&wait_);
                // ws->poll();
                // pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->dispatch(handle_message);
                pthread_mutex_unlock(&wait_);
                break;
            }
            case 2: {
                pthread_mutex_lock(&wait_);
                ws->send("Status:HV?");
                ws->poll();
                pthread_mutex_unlock(&wait_);
                // pthread_mutex_lock(&wait_);
                // ws->poll();
                // pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->dispatch(handle_message);
                pthread_mutex_unlock(&wait_);
                break;
            }
            case 3: {
                pthread_mutex_lock(&wait_);
                ws->send("Status:LED1?");
                ws->poll();
                pthread_mutex_unlock(&wait_);
                // pthread_mutex_lock(&wait_);
                // ws->poll();
                // pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->dispatch(handle_message);
                pthread_mutex_unlock(&wait_);
                break;
            }
            case 4: {
                pthread_mutex_lock(&wait_);
                ws->close();
                pthread_mutex_unlock(&wait_);
                std::cout << "Bye!" << std::endl;
                break;
            }
            default: {
                std::cout << "Not a valid command" << std::endl;
                continue;
            }
        }
    }
    // pthread_mutex_lock(&wait_);
    // ws->poll();
    // pthread_mutex_unlock(&wait_);
    delete ws;
    return 0;
}
